from django.test import Client, TestCase, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from selenium import webdriver
from homepage.apps import HomepageConfig
import unittest
import time

# Create your tests here.
class unitTest(TestCase):
    def test_index_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

#====================================================================================

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()

    def test_page_title(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Story 8", self.driver.title)

    def test_accordion_up(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("commUp").click()
        moved = self.driver.find_element_by_xpath("//div[@id='accordion']/div[@class='card'][2]")
        self.assertIn("commAcc", moved.get_attribute("id"))

    def test_accordion_down(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("orgDown").click()
        moved = self.driver.find_element_by_xpath("//div[@id='accordion']/div[@class='card'][3]")
        self.assertIn("orgAcc", moved.get_attribute("id"))